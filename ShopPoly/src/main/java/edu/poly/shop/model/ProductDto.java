package edu.poly.shop.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.web.multipart.MultipartFile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class ProductDto implements Serializable{

   private Long productId;
	@Column(columnDefinition = "nvarchar(100) not null")
   private String name;
	@Column(nullable = false)
   private int quantity;
	@Column(nullable = false)
   private Double unitPrice;
	@Column(length = 200)
   private String image;
	// private MultipartFile imagefile;
	@Column(columnDefinition = "nvarchar(500) not null")
   private String description;
   @Column(nullable = false)
   private Double discount;
   @Temporal(TemporalType.DATE)
   private Date enteredDate;
   @Column(nullable = false)
   private short status;
   @Column(nullable = false)
   private Long categoryId;
   
   private Boolean isEdit =false;
   
   
}
