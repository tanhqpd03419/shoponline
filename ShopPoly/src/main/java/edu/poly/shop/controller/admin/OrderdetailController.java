package edu.poly.shop.controller.admin;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import edu.poly.shop.domain.Category;

import edu.poly.shop.domain.OrderDetail;

import edu.poly.shop.service.OrderdetailService;

@Controller
@RequestMapping("admin/orderdetails")
public class OrderdetailController {
	@Autowired
	OrderdetailService orderdetailService;
	
	@GetMapping("delete/{orderDetailId}")
	public ModelAndView delete(ModelMap model, @PathVariable("orderDetailId")Integer orderDetailId) {
		orderdetailService.deleteById(orderDetailId);
		model.addAttribute("message","orderDetail is deleted!");
		return new ModelAndView("forward:/admin/orderdetails/search",model);
	}
	
	@RequestMapping("")
	public String list(ModelMap model) {
	List<OrderDetail> list = orderdetailService.findAll();
		model.addAttribute("orderdetails", list);
		
		return "admin/orderdetails/list";
	}
	
	
	

}
