package edu.poly.shop.controller.site;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import edu.poly.shop.domain.Category;
import edu.poly.shop.domain.Product;
import edu.poly.shop.model.CustomerDto;
import edu.poly.shop.service.CategoryService;
import edu.poly.shop.service.CustomerService;
import edu.poly.shop.service.ProductService;

@Controller
@RequestMapping("site")
public class ProductControllerSite {
	
	@Autowired
	ProductService productService;
	@Autowired
	CategoryService categoryService;
	@Autowired
	CustomerService customerService;
	
	@GetMapping("")
	public ModelAndView listpro(ModelMap model) {
		List<Product> listproduct = productService.findAll();
		model.addAttribute("products", listproduct);
		List<Category> listcategory = categoryService.findAll();
		model.addAttribute("categories", listcategory);
		return new ModelAndView( "site/home/home");
	}
@GetMapping("/category/{id}")
	public ModelAndView bycategory(ModelMap model ,@PathVariable Long id) {
		
		model.addAttribute("categories", categoryService.findAll());
		model.addAttribute("products", productService.findByCategoryId(id));
		
		return new ModelAndView( "site/home/home");
	}
@GetMapping("/viewproduct/{id}")
public ModelAndView viewproduct(ModelMap model ,@PathVariable Long id) {
	

	model.addAttribute("product", productService.findById(id).get());
	
	return new ModelAndView( "site/home/viewproduct");
}
	
	@GetMapping("register")
	public String add(Model model) {
		model.addAttribute("customer", new CustomerDto());
		return "site/home/register";
	}
	
	
}
