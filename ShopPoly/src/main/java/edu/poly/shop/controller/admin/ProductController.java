package edu.poly.shop.controller.admin;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.servlet.ServletContext;
import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import edu.poly.shop.domain.Category;
import edu.poly.shop.domain.Product;
import edu.poly.shop.model.CategoryDto;
import edu.poly.shop.model.ProductDto;
import edu.poly.shop.repository.ProductRepository;
import edu.poly.shop.service.CategoryService;
import edu.poly.shop.service.FileUploadService;
import edu.poly.shop.service.ProductService;
import edu.poly.shop.service.impl.FileUploadUtil;

@Controller
@RequestMapping("admin/products")
public class ProductController {
	@Autowired
	ProductService productService;
	@Autowired
	CategoryService CategoryService;
	@Autowired
	FileUploadService fileUploadService;
	
	@Autowired
	ServletContext application;
	
	@ModelAttribute("categorieslist")
	public List<CategoryDto> getCategories(){	
		return CategoryService.findAll().stream().map(item->{
			CategoryDto dto = new CategoryDto();
			BeanUtils.copyProperties(item, dto);
			return dto;
		}).collect(Collectors.toList());
	}
	
	
	@GetMapping("add")
	public String add(Model model) {
		model.addAttribute("product", new ProductDto());
//		model.addAttribute("categorieslist", CategoryService.findAll());
		List<Integer> statusList =Arrays.asList(1,2,3,4,5);
		model.addAttribute("statusList", statusList);
		return "admin/products/addeditproduct";
	}
	@GetMapping("edit/{productId}")
	public ModelAndView edit(ModelMap model, @PathVariable("productId") Long productId) {
		Optional<Product> opt =productService.findById(productId);
		
		ProductDto dto = new ProductDto();
		
		if(opt.isPresent()) {
			Product entity = opt.get();
			
//			Category ct =new Category();
//			ct.setCategoryId(dto.getCategoryId());
//			entity.setCategory(ct);
			
			BeanUtils.copyProperties(entity, dto);
			dto.setCategoryId(entity.getCategory().getCategoryId());
			dto.setIsEdit(true);
			
			model.addAttribute("product",dto);
			//model.addAttribute("categorieslist", ct);
			
		
			return new ModelAndView("admin/products/addeditproduct",model);
		}
		
		model.addAttribute("message","Category is not existed");
		 return new ModelAndView("forward:/admin/products",model);
	}
	@GetMapping("delete/{productId}")
	public ModelAndView delete(ModelMap model, @PathVariable("productId") Long productId) {
		productService.deleteById(productId);
		model.addAttribute("message","product is deleted!");
		return new ModelAndView("forward:/admin/products/search",model);
	}
	@PostMapping("saveOrUpdate")
	public ModelAndView saveOrUpdate(ModelMap model,@Valid @ModelAttribute("product") ProductDto dto ,BindingResult result,
		@RequestParam("file") MultipartFile file) throws IOException {
//		if(result.hasErrors()) {
//			System.out.println(result);
//			return new ModelAndView("admin/products/addeditproduct");
//		}
		Product entity =new Product();
		
//		if(!dto.getImagefile().isEmpty()) {
//			String path = application.getRealPath("/");
//			System.out.println("path" +path);
			
			//try {
//				Path pathObj = Paths.get(path, "/images/");
//				if (!Files.exists(pathObj)) {
//					Files.createDirectories(pathObj);
//				}
//				dto.setImage(dto.getImagefile().getOriginalFilename());
//				
//				String filePath = path + "/images/" +dto.getImage();
//				
//				dto.getImagefile().transferTo(Path.of(filePath));
//				
//				dto.setImagefile(null);
		
		
//			System.out.println("file: " + file.getOriginalFilename());
//			String filePath = fileUploadService.upload(file);
//			dto.setImage(filePath);
		
		
		
//		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
//		if(fileName.contains(".."))
//		{
//			System.out.println("not a a valid file");
//		}
//		try {
//			dto.setImage(Base64.getEncoder().encodeToString(file.getBytes()));
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
			String filename = StringUtils.cleanPath(file.getOriginalFilename());
			String uploadDir ="uploads/";
			FileUploadUtil.savefile(uploadDir, filename, file);
			dto.setImage(filename);
			
				Category ct =new Category();
				ct.setCategoryId(dto.getCategoryId());
				entity.setCategory(ct);
				
				BeanUtils.copyProperties(dto, entity);
				
			
				productService.save(entity);
				
				model.addAttribute("message", "product is save");
			
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
			
		
		return new ModelAndView("redirect:/admin/products/",model);
	}

	@RequestMapping("")
	public String list(ModelMap model) {
		List<Product> list = productService.findAll();
		model.addAttribute("products", list);
		
		return "admin/products/listproduct";
	}
	@GetMapping("search")
	public String search(ModelMap model,@RequestParam(name="name",required = false) String name) {
		List<Product> list = null;
		if(StringUtils.hasText(name)) {
			list =productService.findByNameContaining(name);
		}else {
			list =productService.findAll();
		}
		model.addAttribute("products", list);
		
		return "admin/products/search";
	}
	
	@GetMapping("searchpaginated")
	public String search(ModelMap model,@RequestParam(name="name",required = false) String name,
			@RequestParam("page") Optional<Integer> page,
			@RequestParam("size") Optional<Integer> size) {
		int currentPage =page.orElse(1);
		int pageSize = size.orElse(5);
		
		Pageable pageable =PageRequest.of(currentPage, pageSize,Sort.by("name"));
		Page<Product> resultPage = null;
		
		
		if(StringUtils.hasText(name)) {
			resultPage =productService.findByNameContaining(name,pageable);
			model.addAttribute("name",name);
		}else {
			resultPage =productService.findAll(pageable);
		}
		
		int totalPages= resultPage.getTotalPages();
		if(totalPages>0) {
			int start = Math.max(1, currentPage-2);
			int end =Math.min(currentPage +2, totalPages);
			
			if(totalPages >5) {
				if(end == totalPages) start =end -5;
				else if(start ==1) end =start+5;
			}
			List<Integer>pageNumbers =IntStream.rangeClosed(start, end)
					.boxed()
					.collect(Collectors.toList());
			model.addAttribute("pageNumbers",pageNumbers);
		}
		model.addAttribute("productPage", resultPage);
		
		return "admin/products/searchpaginated";
	}
	
	
	

}
