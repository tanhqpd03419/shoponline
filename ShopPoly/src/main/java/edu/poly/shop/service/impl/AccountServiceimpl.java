package edu.poly.shop.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import edu.poly.shop.domain.Account;
import edu.poly.shop.repository.AccountRepository;
import edu.poly.shop.service.AccountService;
@Service
public class AccountServiceimpl implements AccountService{
 AccountRepository accountRepository;

public AccountServiceimpl(AccountRepository accountRepository) {
	
	this.accountRepository = accountRepository;
}







@Override
public Page<Account> findByUsername(String username, Pageable pageable) {
	return accountRepository.findByUsername(username, pageable);
}







@Override
public List<Account> findByUsername(String username) {
	return accountRepository.findByUsername(username);
}







@Override
public <S extends Account> S save(S entity) {
	return accountRepository.save(entity);
}

@Override
public <S extends Account> Optional<S> findOne(Example<S> example) {
	return accountRepository.findOne(example);
}

@Override
public Page<Account> findAll(Pageable pageable) {
	return accountRepository.findAll(pageable);
}

@Override
public List<Account> findAll() {
	return accountRepository.findAll();
}

@Override
public List<Account> findAll(Sort sort) {
	return accountRepository.findAll(sort);
}

@Override
public List<Account> findAllById(Iterable<String> ids) {
	return accountRepository.findAllById(ids);
}

@Override
public Optional<Account> findById(String id) {
	return accountRepository.findById(id);
}

@Override
public <S extends Account> List<S> saveAll(Iterable<S> entities) {
	return accountRepository.saveAll(entities);
}

@Override
public void flush() {
	accountRepository.flush();
}

@Override
public <S extends Account> S saveAndFlush(S entity) {
	return accountRepository.saveAndFlush(entity);
}

@Override
public boolean existsById(String id) {
	return accountRepository.existsById(id);
}

@Override
public <S extends Account> List<S> saveAllAndFlush(Iterable<S> entities) {
	return accountRepository.saveAllAndFlush(entities);
}

@Override
public <S extends Account> long count(Example<S> example) {
	return accountRepository.count(example);
}

@Override
public <S extends Account> boolean exists(Example<S> example) {
	return accountRepository.exists(example);
}

@Override
public long count() {
	return accountRepository.count();
}

@Override
public void deleteById(String id) {
	accountRepository.deleteById(id);
}

@Override
public void deleteAllByIdInBatch(Iterable<String> ids) {
	accountRepository.deleteAllByIdInBatch(ids);
}

@Override
public void delete(Account entity) {
	accountRepository.delete(entity);
}

@Override
public void deleteAllById(Iterable<? extends String> ids) {
	accountRepository.deleteAllById(ids);
}

@Override
public void deleteAllInBatch() {
	accountRepository.deleteAllInBatch();
}

@Override
public void deleteAll(Iterable<? extends Account> entities) {
	accountRepository.deleteAll(entities);
}

@Override
public void deleteAll() {
	accountRepository.deleteAll();
}

@Override
public Account getById(String id) {
	return accountRepository.getById(id);
}
 
}
