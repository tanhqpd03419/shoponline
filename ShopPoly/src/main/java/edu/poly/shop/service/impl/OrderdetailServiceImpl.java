package edu.poly.shop.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import edu.poly.shop.domain.Order;
import edu.poly.shop.domain.OrderDetail;
import edu.poly.shop.repository.OrderRepository;
import edu.poly.shop.repository.OrderdetailRepository;
import edu.poly.shop.service.OrderdetailService;
@Service
public class OrderdetailServiceImpl implements OrderdetailService {
 OrderdetailRepository orderdetailRepository;

public OrderdetailServiceImpl(OrderdetailRepository orderdetailRepository) {
	
	this.orderdetailRepository = orderdetailRepository;
}

@Override
public <S extends OrderDetail> S save(S entity) {
	return orderdetailRepository.save(entity);
}

@Override
public <S extends OrderDetail> Optional<S> findOne(Example<S> example) {
	return orderdetailRepository.findOne(example);
}

@Override
public Page<OrderDetail> findAll(Pageable pageable) {
	return orderdetailRepository.findAll(pageable);
}

@Override
public List<OrderDetail> findAll() {
	return orderdetailRepository.findAll();
}

@Override
public List<OrderDetail> findAll(Sort sort) {
	return orderdetailRepository.findAll(sort);
}

@Override
public List<OrderDetail> findAllById(Iterable<Integer> ids) {
	return orderdetailRepository.findAllById(ids);
}

@Override
public Optional<OrderDetail> findById(Integer id) {
	return orderdetailRepository.findById(id);
}

@Override
public <S extends OrderDetail> List<S> saveAll(Iterable<S> entities) {
	return orderdetailRepository.saveAll(entities);
}

@Override
public void flush() {
	orderdetailRepository.flush();
}

@Override
public <S extends OrderDetail> S saveAndFlush(S entity) {
	return orderdetailRepository.saveAndFlush(entity);
}

@Override
public boolean existsById(Integer id) {
	return orderdetailRepository.existsById(id);
}

@Override
public <S extends OrderDetail> List<S> saveAllAndFlush(Iterable<S> entities) {
	return orderdetailRepository.saveAllAndFlush(entities);
}

@Override
public <S extends OrderDetail> Page<S> findAll(Example<S> example, Pageable pageable) {
	return orderdetailRepository.findAll(example, pageable);
}

@Override
public void deleteInBatch(Iterable<OrderDetail> entities) {
	orderdetailRepository.deleteInBatch(entities);
}

@Override
public <S extends OrderDetail> long count(Example<S> example) {
	return orderdetailRepository.count(example);
}

@Override
public <S extends OrderDetail> boolean exists(Example<S> example) {
	return orderdetailRepository.exists(example);
}

@Override
public void deleteAllInBatch(Iterable<OrderDetail> entities) {
	orderdetailRepository.deleteAllInBatch(entities);
}

@Override
public long count() {
	return orderdetailRepository.count();
}

@Override
public void deleteById(Integer id) {
	orderdetailRepository.deleteById(id);
}

@Override
public void deleteAllByIdInBatch(Iterable<Integer> ids) {
	orderdetailRepository.deleteAllByIdInBatch(ids);
}

@Override
public void delete(OrderDetail entity) {
	orderdetailRepository.delete(entity);
}

@Override
public void deleteAllById(Iterable<? extends Integer> ids) {
	orderdetailRepository.deleteAllById(ids);
}

@Override
public void deleteAllInBatch() {
	orderdetailRepository.deleteAllInBatch();
}

@Override
public OrderDetail getOne(Integer id) {
	return orderdetailRepository.getOne(id);
}

@Override
public void deleteAll(Iterable<? extends OrderDetail> entities) {
	orderdetailRepository.deleteAll(entities);
}

@Override
public void deleteAll() {
	orderdetailRepository.deleteAll();
}

@Override
public OrderDetail getById(Integer id) {
	return orderdetailRepository.getById(id);
}

@Override
public <S extends OrderDetail> List<S> findAll(Example<S> example) {
	return orderdetailRepository.findAll(example);
}

@Override
public <S extends OrderDetail> List<S> findAll(Example<S> example, Sort sort) {
	return orderdetailRepository.findAll(example, sort);
}




}
