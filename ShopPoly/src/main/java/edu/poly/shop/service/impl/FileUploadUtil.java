package edu.poly.shop.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.springframework.web.multipart.MultipartFile;

public class FileUploadUtil {
  public static void savefile(String uploadDir ,String filename ,MultipartFile file)throws IOException {
	Path path = Paths.get(uploadDir);
	if(!Files.exists(path)) {
		Files.createDirectories(path);
		Files.createDirectories(path);
	}
	try {
		InputStream inputstream =file.getInputStream();
		Path filepath= path.resolve(filename);
		Files.copy(inputstream, filepath,StandardCopyOption.REPLACE_EXISTING);
	} catch (Exception e) {
		// TODO: handle exception
		throw new IOException("count not save file"+filename);
	}
}
}
