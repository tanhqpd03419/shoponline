package edu.poly.shop.service;

import edu.poly.shop.model.CartItem;

public interface CartService {

	int getcount();

	double getAmount();

	void update(int productId, int quantity);

	void claer();

	void remove(int productId);

	void add(CartItem item);

}
