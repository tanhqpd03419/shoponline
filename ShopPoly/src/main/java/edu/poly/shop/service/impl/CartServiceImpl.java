package edu.poly.shop.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;

import edu.poly.shop.model.CartItem;
import edu.poly.shop.service.CartService;
@Service
@SessionScope
public class CartServiceImpl implements CartService {
	private Map<Integer, CartItem> map = new HashMap<Integer,CartItem>();
	 @Override
	public void add(CartItem item) {
		 CartItem existedItem = map.get(item.getProductId());
		 if (existedItem != null) {
			 existedItem.setQuantity(item.getQuantity() + existedItem.getQuantity());
			
		}else {
			map.put(item.getProductId(), item);
		}
		
	}
	 @Override
	public void remove(int productId) {
		 map.remove(productId);
		
	}
	 @Override
	public void claer() {
	    	map.clear();
			
		}
	 @Override
	public void update(int productId,int quantity) {
	    	CartItem item = map.get(productId);
	    	item.setQuantity(quantity + item.getQuantity());
	    	
	    	if(item.getQuantity() <= 0) {
	    		map.remove(productId);
	    	}
			
		}
	 @Override
	public double getAmount() {
			return map.values().stream().mapToDouble(item-> item.getQuantity()*item.getUnitPrice()).sum();
		}
	 @Override
	public int getcount() {
	    	if(map.isEmpty()) {
	    		return 0;
	    	}
	    	return map.values().size();
			
		}
}
